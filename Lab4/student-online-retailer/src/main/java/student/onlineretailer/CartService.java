package student.onlineretailer;

import java.util.Map;

public interface CartService {
    void addItemToCart(int id, int quantity);
    void removeItemFromCart(int id);
    Map<Integer, Integer> getAllItemsInCart();
    double calculateCartCost();
    String getEmail();
    Map<String, Double> calculateTotalCost();
    double calculateSalesTax();
    double calculateSalesTax(double cartCost);
    double calculateDeliveryCharge();
    double calculateDeliveryCharge(double cartCost);

}
