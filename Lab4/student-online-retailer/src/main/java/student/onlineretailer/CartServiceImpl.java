package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartRepository cartRepo;

    @Value("#{catalog}")
    private Map<Integer, Item> catalog;

    @Value("${contactEmail}")
    private String email;

    @Value("${onlineRetailer.salesTaxRate}")
    private double salesTax;

    @Value("${onlineRetailer.deliveryCharge.normal}")
    private double normalDeliveryCharge;

    @Value("${onlineRetailer.deliveryCharge.threshold}")
    private double thresholdDeliveryCharge;

    @Override
    public void addItemToCart(int id, int quantity) {
        if(catalog.containsKey(id)) {
            cartRepo.add(id, quantity);
        }
    }

    public String getEmail() { return email;}

    @Override
    public void removeItemFromCart(int id) {
        cartRepo.remove(id);
    }

    @Override
    public Map<Integer, Integer> getAllItemsInCart() {
        return cartRepo.getAll();
    }

    @Override
    public double calculateCartCost() {

        Map<Integer, Integer> cartItems = cartRepo.getAll();
        double totalCost = 0;

        for (Map.Entry<Integer, Integer> item: cartItems.entrySet()) {
            int id = item.getKey();
            int quantity = item.getValue();
            double itemCost = catalog.get(id).getPrice() * quantity;
            totalCost += itemCost;
        }

        return totalCost;
    }

    public Map<String, Double> calculateTotalCost() {
        double cartCost = calculateCartCost();
        double salesTax = calculateSalesTax(cartCost);
        double deliveryCost = calculateDeliveryCharge(cartCost);
        double totalCost = cartCost+salesTax+deliveryCost;
        double remFreeDelivery = 0;
        if (cartCost > 0 && deliveryCost != 0) remFreeDelivery = thresholdDeliveryCharge-cartCost;
        return Map.of("cartCost", cartCost, "salesTax", salesTax, "deliveryCost", deliveryCost, "totalCost", totalCost, "remFreeDelivery", remFreeDelivery);
    }

    public double calculateSalesTax() {
        return salesTax * calculateCartCost();
    }

    public double calculateSalesTax(double cartCost) {
        return salesTax * cartCost;
    }

    public double calculateDeliveryCharge() {
        double cartCost = calculateCartCost();
        if (cartCost == 0 || cartCost > thresholdDeliveryCharge) {
            return 0;
        }
        return normalDeliveryCharge;
    }

    public double calculateDeliveryCharge(double cartCost) {
        if (cartCost == 0 || cartCost > thresholdDeliveryCharge) {
            return 0;
        }
        return normalDeliveryCharge;
    }
}
