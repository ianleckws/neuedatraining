package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.catalog.Catalog;
import java.util.Map;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartRepository cartRepo;

    @Autowired
    private Map<Integer, Item> catalog;

    @Override
    public void addItemToCart(int id, int quantity) {
        if(catalog.containsKey(id)) {
            cartRepo.add(id, quantity);
        }
    }

    @Override
    public void removeItemFromCart(int id) {
        cartRepo.remove(id);
    }

    @Override
    public Map<Integer, Integer> getAllItemsInCart() {
        return cartRepo.getAll();
    }

    @Override
    public double calculateCartCost() {

        Map<Integer, Integer> cartItems = cartRepo.getAll();
        double totalCost = 0;

        for (Map.Entry<Integer, Integer> item: cartItems.entrySet()) {
            int id = item.getKey();
            int quantity = item.getValue();
            double itemCost = catalog.get(id).getPrice() * quantity;
            totalCost += itemCost;
        }

        return totalCost;
    }
}
