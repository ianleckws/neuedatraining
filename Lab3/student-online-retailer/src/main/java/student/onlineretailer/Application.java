package student.onlineretailer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {

		ApplicationContext ctx = SpringApplication.run(Application.class, args);

		CartService cartSvc = ctx.getBean(CartServiceImpl.class);

		cartSvc.addItemToCart(0, 100);
		cartSvc.addItemToCart(1, 100);
		cartSvc.addItemToCart(2, 100);
		cartSvc.addItemToCart(3, 100);
		cartSvc.addItemToCart(4, 100);

		cartSvc.removeItemFromCart(1);

		cartSvc.addItemToCart(1, 9);

		double cost = cartSvc.calculateCartCost();
		System.out.printf("Total cost billable is $%.2f\n", cost);
	}

	@Bean
	public Map<Integer, Item> catalog() {

		Map<Integer, Item> inventory = new HashMap<>();

		inventory.put(0, new Item(0, "Apples", 0.99));
		inventory.put(1, new Item(1, "Durian", 14.99));
		inventory.put(2, new Item(2, "Grapes", 2.99));
		inventory.put(3, new Item(3, "Guava", 2.49));
		inventory.put(4, new Item(4, "Kiwi", 3.49));
		inventory.put(5, new Item(5, "Oranges", 0.49));
		return inventory;
	}
}
